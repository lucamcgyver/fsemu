# README #

This libray is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.

### About FSEmu ###

This library aims to implement a basic File System emulator to address the most basic issues such as (but not limited to)

* Organising files inside the JVM memory, for a fs-like hierarchy of resources

* Persisting those data in a serialized file written on the local file system and retrieving the whole file tree as it was

* Having an easy way to test applications that need file saving without actually writing anything to your local disk

### How do I get set up? ###

All the setup for developing should be done by maven, so you just need to import the repository to your favorite IDE and work from there.
If you just need the library as a .jar file look in the download section of the repository or in the project webpage.

### Contribution guidelines ###

If you want to contribute, please remember to **always branch** before doing anything. Commits will be examined and incorporated where needed!

### Contact informations ###
* Project website: [http://ldsoftware.it/fsemu](http://ldsoftware.it/fsemu) (not set up yet)

* Mantainer: Luca Di Stefano: See contact info on website [http://ldsoftware.it](http://ldsoftware.it) - Still to be updated