/*
 * This file is part of FSEmu.
 * 
 * FSEmu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, version 3 of the License.
 * 
 * FSEmu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with FSEmu. If not, see <a href="">http://www.gnu.org/licenses/</a>
 */

package it.ldsoftware.fsemu;

import java.io.File;

import it.ldsoftware.fsemu.filesystem.FSEntry;
import it.ldsoftware.fsemu.filesystem.FSFileSystem;
import it.ldsoftware.fsemu.io.FSInputOutput;

import org.junit.Test;

public class SavingTest {
	private FSFileSystem fileSystem;
	
	public void testSave() {
		fileSystem = new FSFileSystem("testDrive", "/var/run/media/luca/EXT4/data/", false);
		FSEntry root = fileSystem.getRoot();
		
		FSEntry dir01 = fileSystem.createEntity(root, true, "dir01");
		fileSystem.createEntity(dir01, false, "file.pdf");
		
		FSInputOutput.writeFileSystem(fileSystem);
		
		String fullPath = fileSystem.getBasePath() + 
				File.separator + fileSystem.getDriveName() + FSFileSystem.DRIVE_EXT;
		File f = new File(fullPath);
		assert f.exists();
	}
	
	@Test
	public void testRead() {
		testSave();
		
		String fullPath = fileSystem.getBasePath() + 
				File.separator + fileSystem.getDriveName() + FSFileSystem.DRIVE_EXT;
		
		FSFileSystem loaded = FSInputOutput.loadFile(fullPath);
		loaded.toString();
	}
	
}
