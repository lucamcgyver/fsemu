/*
 * This file is part of FSEmu.
 * 
 * FSEmu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, version 3 of the License.
 * 
 * FSEmu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with FSEmu. If not, see <a href="">http://www.gnu.org/licenses/</a>
 */
package it.ldsoftware.fsemu.io;

import it.ldsoftware.fsemu.filesystem.FSFileSystem;
import it.ldsoftware.fsemu.interfaces.IFSEntry;

import java.io.File;

/**
 * 
 * @author luca
 *
 */
public class EntityImporter {
	
	public static void importFile(File toImport, IFSEntry destination, FSFileSystem drive) {
		// TODO
	}
	
	public static void importFile(String toImport, String destination, FSFileSystem drive) {
		importFile(new File(toImport), drive.getEntry(destination), drive);
	}
	
}
