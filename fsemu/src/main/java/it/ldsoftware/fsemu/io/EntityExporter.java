/*
 * This file is part of FSEmu.
 * 
 * FSEmu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, version 3 of the License.
 * 
 * FSEmu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with FSEmu. If not, see <a href="">http://www.gnu.org/licenses/</a>
 */
package it.ldsoftware.fsemu.io;

import it.ldsoftware.fsemu.filesystem.FSFileSystem;
import it.ldsoftware.fsemu.interfaces.IFSEntry;

import java.io.File;

/**
 * 
 * @author luca
 *
 */
public class EntityExporter {
	
	public static void exportEntry(IFSEntry entry, File to, FSFileSystem drive) {
		// TODO
	}
	
	public static void exportEntry(String entry, String to, FSFileSystem drive) {
		exportEntry(drive.getEntry(entry), new File(to), drive);
	}
	
}
