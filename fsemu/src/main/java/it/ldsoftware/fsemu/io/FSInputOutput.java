/*
 * This file is part of FSEmu.
 * 
 * FSEmu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, version 3 of the License.
 * 
 * FSEmu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with FSEmu. If not, see <a href="">http://www.gnu.org/licenses/</a>
 */
package it.ldsoftware.fsemu.io;

import it.ldsoftware.fsemu.filesystem.FSFileSystem;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * 
 * @author luca
 *
 */

public class FSInputOutput {
	
	public static FSFileSystem loadFile(String file) {
		try {
			FileInputStream fs = new FileInputStream(file);
			ObjectInputStream is = new ObjectInputStream(fs);
			FSFileSystem fileSystem = (FSFileSystem) is.readObject();
			fs.close();
			is.close();
			return fileSystem;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void writeFileSystem(FSFileSystem fs) {
		String to = fs.getBasePath() + File.separator + fs.getDriveName() + FSFileSystem.DRIVE_EXT;
		try {
			FileOutputStream file = new FileOutputStream(to);
			BufferedOutputStream buffer = new BufferedOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(buffer);
			out.writeObject(fs);
			
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}