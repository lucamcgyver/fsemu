/*
 * This file is part of FSEmu.
 * 
 * FSEmu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, version 3 of the License.
 * 
 * FSEmu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with FSEmu. If not, see <a href="">http://www.gnu.org/licenses/</a>
 */
package it.ldsoftware.fsemu.filesystem;

import it.ldsoftware.fsemu.interfaces.IFSEntry;

/**
 * 
 * @author luca
 *
 */
public class FSLink implements IFSEntry {
	
	private static final long serialVersionUID = 1L;
	
	private IFSEntry linked;
	private IFSEntry parent;
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		if (parent != null)
			sb.append(parent.toString());
		sb.append(linked.getEntryName());
		sb.append((linked.isDirectory() ? "/": ""));
		
		return sb.toString();
	}

	@Override
	public int compareTo(IFSEntry o) {
		return linked.compareTo(o);
	}

	@Override
	public boolean isDirectory() {
		return linked.isDirectory();
	}

	@Override
	public long getSize() {
		return linked.getSize();
	}

	@Override
	public IFSEntry getParent() {
		return parent;
	}

	@Override
	public void remChild(IFSEntry entry) {
		linked.remChild(entry);
	}

	@Override
	public void addChild(IFSEntry entry) {
		linked.addChild(entry);
	}

	@Override
	public void setParent(IFSEntry parent) {
		this.parent = parent;
	}

	@Override
	public String getEntryName() {
		return linked.getEntryName();
	}

	public IFSEntry getLinked() {
		return linked;
	}

	public void setLinked(IFSEntry toLink) {
		this.linked = toLink;
	}

	@Override
	public Byte[] getData() {
		return linked.getData();
	}

}
