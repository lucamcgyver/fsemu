/*
 * This file is part of FSEmu.
 * 
 * FSEmu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, version 3 of the License.
 * 
 * FSEmu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with FSEmu. If not, see <a href="">http://www.gnu.org/licenses/</a>
 */
package it.ldsoftware.fsemu.filesystem;

import it.ldsoftware.fsemu.interfaces.IFSEntry;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author luca
 *
 */
public class FSEntry implements IFSEntry {
	private static final long serialVersionUID = 1L;
	
	private String entryName;
	private Byte[] data;
	private boolean directory, memory = false, locked = false;
	private Date creation, lastEdit;
	private List<FSEntry> history = new LinkedList<FSEntry>();
	private IFSEntry parent;
	private List<IFSEntry> children = new ArrayList<IFSEntry>();
	
	public FSEntry(String name, boolean directory) {
		entryName = name;
		creation = new Date();
		lastEdit = new Date();
		this.directory = directory;
	}
	
	public FSEntry(String name, boolean directory, boolean hasMemory) {
		this(name, directory);
		memory = hasMemory;
	}
	
	public void archive() {
		FSEntry old = new FSEntry(entryName, directory, memory);
		old.setData(data);
		old.setLocked(locked);
		old.setCreation(creation);
		old.setLastEdit(lastEdit);
		history.add(old);
	}
	
	public void addChild(IFSEntry entry) {
		children.add(entry);
	}
	
	public void remChild(IFSEntry entry) {
		children.remove(entry);
	}
	
	@Override
	public int compareTo(IFSEntry o) {
		return this.entryName.compareToIgnoreCase(o.getEntryName());
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		if (parent != null)
			sb.append(parent.toString());
		sb.append(entryName);
		sb.append((directory ? "/": ""));
		
		return sb.toString();
	}
	
	public String toDetailedString() {
		StringBuilder sb = new StringBuilder();
		// TODO
		sb.append("- ");
		sb.append((directory? "directory" : "file"));
		sb.append(": ");
		sb.append(entryName);
		
		return sb.toString();
	}
	
	public long getSize() {
		long size = 0L;
		if (directory) {
			for (IFSEntry child: children) {
				size += child.getSize();
			}
		} else {
			if (data != null)
				size = data.length;
		}
		return size;
	}

	public String getEntryName() {
		return entryName;
	}

	public void setEntryName(String entryName) {
		this.entryName = entryName;
	}

	public Byte[] getData() {
		return data;
	}

	public void setData(Byte[] data) {
		this.data = data;
	}

	public boolean isDirectory() {
		return directory;
	}

	public void setDirectory(boolean folder) {
		this.directory = folder;
	}

	public boolean isMemory() {
		return memory;
	}

	public void setMemory(boolean memory) {
		this.memory = memory;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Date getLastEdit() {
		return lastEdit;
	}

	public void setLastEdit(Date lastEdit) {
		this.lastEdit = lastEdit;
	}

	public List<FSEntry> getHistory() {
		return history;
	}

	public void setHistory(List<FSEntry> history) {
		this.history = history;
	}

	public IFSEntry getParent() {
		return parent;
	}

	public void setParent(IFSEntry parent) {
		this.parent = parent;
	}

	public List<IFSEntry> getChildren() {
		return children;
	}

	public void setChildren(List<IFSEntry> children) {
		this.children = children;
	}
	
}