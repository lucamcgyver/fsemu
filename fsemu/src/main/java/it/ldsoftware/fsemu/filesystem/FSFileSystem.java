/*
 * This file is part of FSEmu.
 * 
 * FSEmu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, version 3 of the License.
 * 
 * FSEmu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with FSEmu. If not, see <a href="">http://www.gnu.org/licenses/</a>
 */
package it.ldsoftware.fsemu.filesystem;

import it.ldsoftware.fsemu.interfaces.IFSEntry;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author luca
 *
 */
public class FSFileSystem implements Serializable {
	public static final String DRIVE_EXT = ".edsk";

	private static final long serialVersionUID = 1L;
	
	private Map<String, IFSEntry> allEntries = new HashMap<String, IFSEntry>();
	private FSEntry root;
	private boolean memory;
	
	private String driveName, basePath;
	private File realFile;
	
	public FSFileSystem(String driveName, String basePath, boolean trackHistory) {
		memory = trackHistory;
		this.driveName = driveName;
		this.basePath = basePath;
		realFile = new File(basePath + File.separator + driveName + DRIVE_EXT);
		root = new FSEntry("", true);
	}
	
	/*
	 * Information methods
	 */
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("FSEmu FileSystem: ");
		sb.append(driveName);
		
		return sb.toString();
	}
	
	public String toDetailedString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("/");
		// TODO
		return sb.toString();
	}
	
	public long getDriveSize() {
		long size = 0L;
		
		Iterator<Entry<String, IFSEntry>> it = allEntries.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, IFSEntry> entry = it.next();
			IFSEntry file = entry.getValue();
			
			if (!file.isDirectory()) {
				size += file.getSize();
			}
		}
		
		return size;
	}
	
	/*
	 * Working with entries 
	 */
	
	public IFSEntry getEntry(String path) {
		return allEntries.get(path);
	}
	
	public synchronized FSEntry createEntity(FSEntry destination, boolean directory, String name, boolean hasMemory) {
		if (destination.isDirectory()) {
			FSEntry entry = new FSEntry(name, directory, hasMemory);
			
			entry.setParent(destination);
			destination.addChild(entry);
			
			allEntries.put(entry.toString(), entry);
			
			return entry;
		}
		return null;
	}
	
	public synchronized FSEntry createEntity(FSEntry destination, boolean directory, String name) {
		return createEntity(destination, directory, name, memory);
	}
	
	public void writeEntry(FSEntry entry, Byte[] data) {
		synchronized (entry) {
			entry.archive();
			entry.setData(data);
			entry.setLastEdit(new Date());
		}
	}
	
	public FSEntry copyEntry(IFSEntry toCopy, IFSEntry destination) {
		return null; // TODO
	}
	
	public FSEntry copyEntry(String toCopy, String destination) {
		synchronized (allEntries) {
			return copyEntry(getEntry(toCopy), getEntry(destination));
		}
	}
	
	public void linkEntry(IFSEntry toLink, IFSEntry destination) {
		if (destination.isDirectory()) {
			FSLink link = new FSLink();
			link.setLinked(toLink);
			link.setParent(destination);
			
			allEntries.put(link.toString(), link);
		}
	}
	
	public void linkEntry(String toLink, String destination) {
		synchronized (allEntries) {
			linkEntry(allEntries.get(toLink), allEntries.get(destination));
		}
	}
	
	public void moveEntry(IFSEntry toMove, IFSEntry destination) {
		if (destination.isDirectory()) {
			synchronized (toMove) {
				synchronized (destination) {
					allEntries.remove(toMove);
					toMove.getParent().remChild(toMove);
					
					destination.addChild(toMove);
					toMove.setParent(destination);
					allEntries.put(toMove.toString(), toMove);
				}
			}
		}
	}
	
	public void moveEntry(String toMove, String destination) {
		synchronized(allEntries) {
			moveEntry(getEntry(toMove), getEntry(destination));
		}
	}

	/*
	 * GETTERS AND SETTERS
	 */
	
	public Map<String, IFSEntry> getAllEntries() {
		return allEntries;
	}

	public void setAllEntries(Map<String, IFSEntry> allEntries) {
		this.allEntries = allEntries;
	}

	public FSEntry getRoot() {
		return root;
	}

	public void setRoot(FSEntry root) {
		this.root = root;
	}

	public boolean isMemory() {
		return memory;
	}

	public void setMemory(boolean memory) {
		this.memory = memory;
	}

	public String getDriveName() {
		return driveName;
	}

	public void setDriveName(String driveName) {
		this.driveName = driveName;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public File getRealFile() {
		return realFile;
	}

	public void setRealFile(File realFile) {
		this.realFile = realFile;
	}
}